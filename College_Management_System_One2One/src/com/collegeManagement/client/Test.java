package com.collegeManagement.client;

import java.util.Scanner;

import com.collegeManagement.serviceImpl.Karvenagar;

public class Test {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Karvenagar kv = new Karvenagar();
		
		System.out.println("*****Select********\n"
				+ "1. add Course \n"
				+ "2. view Course \n"
				+ "3. add Faculty\n"
				+ "4. view Faculty \n"
				+ "5. add Batch \n"
				+ "6. view Batch \n"
				+ "7. add Student \n"
				+ "8. view Student\n");
		int ch = sc.nextInt();
		
		switch (ch) {
		case 1 :
			kv.addCourse();
			break;
			
		case 2 : 
			System.out.println("Enter Course Id : ");
			int cid = sc.nextInt();
			kv.viewCourse(cid);
			break;
			
		case 3 : 
			kv.addFaculty();
			break;
			
		case 4 :
			System.out.println("Enter Faculty Id : ");
			int fid = sc.nextInt();
			kv.viewFaculty(fid);
			break;

		default:
			break;
		}
		
	}
	
}
