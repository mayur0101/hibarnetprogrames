package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Employee {
	@Id
	private int eid;
	private String ename;
	private float salary;
	private long mobNo;
	private String addr;	
	
	public Employee() {
			}

	public Employee(int eid, String ename, float salary, long mobNo, String addr) {
		this.eid = eid;
		this.ename = ename;
		this.salary = salary;
		this.mobNo = mobNo;
		this.addr = addr;
	}

	public int getEid() {
		return eid;
	}
	public String getEname() {
		return ename;
	}
	public float getSalary() {
		return salary;
	}
	public long getMobNo() {
		return mobNo;
	}
	public String getAddr() {
		return addr;
	}

}
