package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;


@NamedNativeQueries({
	@NamedNativeQuery(name = "insertData" , query = "insert into Student values(?, ?)"),
	@NamedNativeQuery(name = "getAllData", query = "Select * from Student", resultClass = Student.class)
})
@Entity
public class Student {
	@Id
	private int rollNo;
	private String name;
	
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
