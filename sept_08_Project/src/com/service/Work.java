package com.service;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.helper.HibernateUtil;
import com.model.User;

public class Work implements Task {
	
	SessionFactory sf = HibernateUtil.getSessionFactory();
	Scanner sc = new Scanner(System.in);

	@Override
	public void addUser() {
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		System.out.println("Enter User Id : ");
		int id = sc.nextInt();
		System.out.println("Enter User Name : ");
		String name = sc.next();
		System.out.println("Enter User PassWord : ");
		String pass = sc.next();
		System.out.println("Enter User Address : ");
		String addr = sc.next();
		
		Query<User> query = s.getNamedNativeQuery("addUser");
		query.setParameter(0, id);
		query.setParameter(1, name);
		query.setParameter(2, pass);
		query.setParameter(3, addr);
		
		query.executeUpdate();
		tx.commit();
		
	}

	@Override
	public void getSingleUser(int id) {
		Session s = sf.openSession();
		
		Query<User> query = s.getNamedQuery("getSingleUser");
		query.setParameter(0, id);
		User u = query.getSingleResult();
		System.out.println(u.getId());
		System.out.println(u.getName());
		System.out.println(u.getPass());
		System.out.println(u.getAddr());
		
		
	}

	@Override
	public void getAllUser() {
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		Query<User> query = s.getNamedQuery("getAllUser");
		List<User> ulist = query.getResultList();
		ulist.forEach(x-> {
			System.out.println(x.getId());
			System.out.println(x.getName());
			System.out.println(x.getPass());
			System.out.println(x.getAddr());
		});
		
	}

	@Override
	public void updateUser() {
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		System.out.println("Enter User Id to View : ");
		int id = sc.nextInt();
		getSingleUser(id);
		Query<User> query;
		
		boolean b = true;
		while(b)
		{
			System.out.println("******Select********\n"
					+ "1. for name \n"
					+ "2. for PassWord \n"
					+ "3. for Address \n "
					+ "4. Break");
			int ch = sc.nextInt();
			
			if(ch==1)
			{
				Session s1 = sf.openSession();
				Transaction tx1 = s1.beginTransaction();
				query = s1.getNamedQuery("updateName");
				System.out.println("Enter New Name : ");
				String name = sc.next();
				query.setParameter(0, name);
				query.setParameter(1, id);
				query.executeUpdate();
				tx1.commit();				
			}
			else if(ch==2)
			{
				Session s2 = sf.openSession();
				Transaction tx2 = s2.beginTransaction();
				query = s2.getNamedQuery("updatePass");
				System.out.println("Enter new passWord : ");
				String pass = sc.next();
				query.setParameter(0, pass);
				query.setParameter(1, id);
				query.executeUpdate();
				tx2.commit();
			}
			else if (ch==3)
			{
				Session s3 = sf.openSession();
				Transaction tx3 = s3.beginTransaction();
				query = s3.getNamedQuery("updateAddr");
				System.out.println("Enter new Address : ");
				String addr = sc.next();
				query.setParameter(0, addr);
				query.setParameter(1, id);
				query.executeUpdate();
				tx3.commit();
			}
			else if(ch==4)
			{
				b=false;
			}
			else
			{
				System.out.println("Invalid Choice..!!");
			}	
		}	
	}

	@Override
	public void deleteUser() {
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		Query<User> query = s.getNamedQuery("deleteuser");
		System.out.println("Enter User Id : ");
		int id = sc.nextInt();
		query.setParameter(0, id);
		query.executeUpdate();
		tx.commit();
		
	}

}
