package com;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

public class TestGetSingleData {
	
	public static void main(String[] args) {
//		Select * from User where uid = ?
		Session sess = HibernateUtil.getSessionFactory().openSession();
		
		CriteriaBuilder cb = sess.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> userRoot = cq.from(User.class);
		cq.where(cb.equal(userRoot.get("uid"), 2));
		
		Query<User> query = sess.createQuery(cq);
		User u = query.getSingleResult();
		System.out.println(u.getUid());
		System.out.println(u.getName());
		
	}

}
