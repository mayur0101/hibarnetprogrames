package com.service;

import java.util.List;
import java.util.Scanner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.help.HibernateUtil;
import com.model.Employee;

public class Data {
	
	Scanner sc = new Scanner(System.in);
	SessionFactory sf = HibernateUtil.getSessionFactory();
	
	public void addData()
	{
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		System.out.println("Enter Employee Id : ");
		int eid = sc.nextInt();
		System.out.println("Enter Employee Name : ");
		String ename = sc.next();
		System.out.println("Enter Employee Salary : ");
		float salary = sc.nextFloat();
		System.out.println("Enter Employee Mobile No : ");
		long mobNo = sc.nextLong();
		System.out.println("Enter Employee Address : ");
		String addr = sc.next();
		
		Employee e = new Employee(eid, ename, salary, mobNo, addr);
		sess.save(e);
		tx.commit();
		
	}
	
	public void getAllData()
	{
//		Select * from Employee
		Session sess = sf.openSession();
		CriteriaBuilder cb = sess.getCriteriaBuilder();
		CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
		Root<Employee> empRoot = cq.from(Employee.class);
		cq.select(empRoot);
		Query<Employee> query = sess.createQuery(cq);
		List<Employee> elist = query.getResultList();
		elist.forEach(e-> {
			System.out.println(e.getEid());
			System.out.println(e.getEname());
			System.out.println(e.getSalary());
			System.out.println(e.getMobNo());
			System.out.println(e.getAddr());
		});
	}

}
