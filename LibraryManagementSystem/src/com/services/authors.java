package com.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import DatabaseConnector.ConnectionProvider;

public class authors implements LibraryDataOperations {
    private String authorName;
    private String nationality;

    @Override
    public void add_data() {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter Name of Author: ");
            authorName = scanner.next(); //.trim(); // Read a whole line and trim any leading/trailing spaces
            System.out.print("Enter Nationality of Author: ");
            nationality = scanner.next(); //Line().trim();

            if (authorName.isEmpty() || nationality.isEmpty()) {
                System.out.println("Author name and nationality cannot be empty.");
                return;
            }

            String insertAuthorSQL = "INSERT INTO Authors (AuthorName, Nationality) VALUES (?, ?)";

            try {
            	Class.forName ("com.mysql.cj.jdbc.Driver");
          	   final String JDBC_URL = "jdbc:mysql://localhost:3306/lms?characterEncoding=utf8";
          	
          	 Connection connection=DriverManager.getConnection(JDBC_URL,"root","root");
          	 PreparedStatement preparedStatement = connection.prepareStatement(insertAuthorSQL);
                preparedStatement.setString(1, authorName);
                preparedStatement.setString(2, nationality);

                int rowsInserted = preparedStatement.executeUpdate();

                if (rowsInserted > 0) {
                    System.out.println("Author added successfully!");
                } else {
                    System.out.println("Failed to add author.");
                }
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
            	System.out.println(e);
            } catch (ClassNotFoundException e) {
            	System.out.println(e);
			}
        }
    }
}
