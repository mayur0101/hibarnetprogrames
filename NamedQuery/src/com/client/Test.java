package com.client;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.config.HibernateUtil;
import com.model.Student;



public class Test {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		
		System.out.println("****************Get All Data*****************");
		
		Query<Student> namedQuery = session.getNamedQuery("getAllData");
		
		List<Student> slist = namedQuery.getResultList();
		slist.forEach(x-> {
			System.out.println(x.getRollNo());
			System.out.println(x.getName());
		});
		
		
		System.out.println("****************Update Data*****************");
		
		System.out.println("Enter Roll No : ");
		int rollNo = sc.nextInt();
		System.out.println("Enter new Name : ");
		String name = sc.next();
		Query<Student> query = session.getNamedQuery("updateData");
		query.setParameter("nm", name);
		query.setParameter("rn", rollNo);
		query.executeUpdate();
		tx.commit();
		
		System.out.println("****************Get Single data***************");
		
		System.out.println("Enter Roll No : ");
		int rollNo2 = sc.nextInt();
	
		Query<Student> query2 = session.getNamedQuery("getSingleData");
		query2.setParameter("rn", rollNo2);
		Student stu = query2.getSingleResult();
		System.out.println(stu.getRollNo());
		System.out.println(stu.getName());
		
		
		
		
		
	}

}
