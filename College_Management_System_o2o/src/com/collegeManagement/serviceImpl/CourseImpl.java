package com.collegeManagement.serviceImpl;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.collegeManagement.config.HibernateUtil;
import com.collegeManagement.model.Course;
import com.collegeManagement.service.Cjc;

public class CourseImpl implements Cjc {
	
	Scanner sc = new Scanner(System.in);
	SessionFactory sf = HibernateUtil.getSessionFactory();

	@Override
	public void add() {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		
		System.out.println("Enter Course Id : ");
		int cid = sc.nextInt();
		System.out.println("Enter Course Name : ");
		String cname = sc.next();
		
		Query<Course> query = sess.getNamedQuery("addCourse");
		query.setParameter(0, cid);
		query.setParameter(1, cname);
		query.executeUpdate();
		tx.commit();
		
	}

	@Override
	public void getSingle() {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		System.out.println("Enter Course Id : ");
		int cid = sc.nextInt();
		Query<Course> query = sess.getNamedQuery("getSingleCourse");
		query.setParameter(0, cid);
		Course c = query.getSingleResult();
		System.out.println(c.getCid());
		System.out.println(c.getCname());
		
	}

	@Override
	public void getAll() {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Query<Course> query = sess.getNamedQuery("getAll");
		List<Course> clist = query.getResultList();
		clist.forEach(x-> {
			System.out.println(x.getCid());
			System.out.println(x.getCname());
		});
		
	}

	@Override
	public void update() {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		System.out.println("Enter Course Id : ");
		int cid = sc.nextInt();
		System.out.println("Enter New Course Name : ");
		String cname = sc.next();
		Query<Course> query = sess.getNamedQuery("updateCourse");
		query.setParameter(0, cname);
		query.setParameter(1, cid);
		query.executeUpdate();
		tx.commit();
		
		
	}

	@Override
	public void delete() {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		System.out.println("Enter Course Id : ");
		int cid = sc.nextInt();
		Query<Course> query = sess.getNamedQuery("deleteCourse");
		query.setParameter(0, cid);
		query.executeUpdate();
		tx.commit();
		
	}
	

}
