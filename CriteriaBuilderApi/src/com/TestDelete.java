package com;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class TestDelete {
	
	public static void main(String[] args) {
//		delete from User where uid = ?
		Session sess = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sess.beginTransaction();
		CriteriaBuilder cb = sess.getCriteriaBuilder();
		CriteriaDelete<User> cd = cb.createCriteriaDelete(User.class);
		Root<User> userRoot = cd.from(User.class);
		cd.where(cb.equal(userRoot.get("uid"), 3));
		
		Query<User> query = sess.createQuery(cd);
		query.executeUpdate();
		tx.commit();
		
		
	}

}
