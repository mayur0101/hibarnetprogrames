package com.collegeManagement.serviceImpl;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.collegeManagement.config.HibernateUtil;
import com.collegeManagement.model.Batch;
import com.collegeManagement.model.Course;
import com.collegeManagement.model.Faculty;
import com.collegeManagement.service.Cjc;

public class Karvenagar implements Cjc {
	
	Scanner sc = new Scanner(System.in);
	SessionFactory sf = HibernateUtil.getSessionFactory();

	@Override
	public void addCourse() {
		
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Course c = new Course();
		
		System.out.println("Enter Course Id : ");
		c.setCid(sc.nextInt());
		System.out.println("Enter Course Name : ");
		c.setCname(sc.next());
		sess.save(c);
		tx.commit();
		
		
	}

	@Override
	public void viewCourse(int cid) {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Course c = sess.get(Course.class, cid);
		System.out.println(c.getCid());
		System.out.println(c.getCname());
		
	}

	@Override
	public void addFaculty() {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Faculty f = new Faculty();
		Course c = new Course();
		System.out.println("Enter Faculty Id : ");
		f.setFid(sc.nextInt());
		System.out.println("Enter Faculty Name : ");
		f.setFname(sc.next());
		List<Course> clist = sess.createQuery("from Course").getResultList();
		System.out.println("These are the Courses Available..");
		clist.forEach(x-> {
			System.out.println(x.getCid());
			System.out.println(x.getCname());
		});
		System.out.println("********Select****** \n"
				+ "1. to add Existing Course. \n"
				+ "2. to add new Course. \n");
		int ch = sc.nextInt();
		if(ch==1)
		{
			System.out.println("Enter Course Id to set : ");
			int cid = sc.nextInt();
			c = sess.get(Course.class, cid);
			f.setCourse(c);
		}
		else if(ch==2)
		{
			System.out.println("Enter Course Id : ");
			c.setCid(sc.nextInt());
			System.out.println("Enter Course Name : ");
			c.setCname(sc.next());
			f.setCourse(c);
		}
		else
		{
			System.out.println("Invalid Choice..!!-----");
		}
		sess.save(f);
		tx.commit();
		
		
	}

	@Override
	public void viewFaculty(int fid) {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Faculty f = sess.get(Faculty.class, fid);
		System.out.println(f.getFid());
		System.out.println(f.getFname());
		System.out.println(f.getCourse().getCid());
		System.out.println(f.getCourse().getCname());
		
	}

	@Override
	public void addBatch() {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Batch b = new Batch();
		Faculty f = new Faculty();
		System.out.println("Enter batch Id : ");
		b.setBid(sc.nextInt());
		System.out.println("Enter batch Name : ");
		b.setBname(sc.next());
		List<Faculty> flist = sess.createQuery("from Faculty").getResultList();
		System.out.println("These are the Faculty available..");
		flist.forEach(x-> {
			System.out.println(x.getFid());
			System.out.println(x.getFname());
		});
		System.out.println("********Select****** \n"
				+ "1. to add Existing faculty. \n"
				+ "2. to add new Faculty. \n");
		int ch = sc.nextInt();
		
		if(ch==1)
		{
			System.out.println("Enter Faculty Id to set : ");
			int fid = sc.nextInt();
			f = sess.get(Faculty.class, fid);
			b.setFaculty(f);
		}
		else if (ch==2)
		{
			addFaculty(); 
		}
		
	}

	@Override
	public void viewBatch(int bid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addStudent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void viewStudent(int sid) {
		// TODO Auto-generated method stub
		
	}
	
	
	

}
