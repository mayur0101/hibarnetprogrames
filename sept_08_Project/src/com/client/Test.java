package com.client;

import java.util.Scanner;

import com.service.Work;

public class Test {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Work w = new Work();
		while(true)
		{
			System.out.println("******Select******* \n"
					+ "1. Add User \n"
					+ "2. Get Single User \n"
					+ "3. Get All User \n"
					+ "4. Update user \n"
					+ "5. Delete User \n"
					+ "6. Exit.");
			int ch = sc.nextInt();
			
			switch (ch) {
			case 1 :
					w.addUser();
					System.out.println("User Added Successfully..!!");
				break;
				
			case 2 :
				System.out.println("Enter user Id : ");
				int id = sc.nextInt();
				w.getSingleUser(id);		
			break;
			
			case 3 :
				w.getAllUser(); 		
			break;
			
			case 4 :
				w.updateUser();	
				System.out.println("User Updated..!!");
			break;
			
			case 5 :
				w.deleteUser();	
				System.out.println("User Deleted..!!");
			break;
			
			case 6 :
				System.exit(ch);
			break;
				
	
			default:
				System.out.println("Invalid Choice..!!");
				break;
			}
		}
		
	}
	
}
