package com;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

public class Test {
	
	public static void main(String[] args) {
		
		Session sess = HibernateUtil.getSessionFactory().openSession();
		
		User u = new User();
		
		Query<User> query = sess.createQuery("Select u from User u");
		List<User> ulist = query.getResultList();
		ulist.forEach(x-> {
			System.out.println(x.getUid());
			System.out.println(x.getName());
		});
		
		
	}

}
