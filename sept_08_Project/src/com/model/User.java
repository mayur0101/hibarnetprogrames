package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(name = "getSingleUser", query = "from User Where id = ?"),
	@NamedQuery(name = "getAllUser", query = "from User"),
	@NamedQuery(name = "updateName", query = "Update User set name = ? Where id = ?"),
	@NamedQuery(name = "updatePass", query = "Update User set pass = ? Where id = ?"),
	@NamedQuery(name = "updateAddr", query = "Update User set addr = ? Where id = ?"),
	@NamedQuery(name = "deleteuser", query = "delete from User Where id = ?")	
})
@NamedNativeQuery(name = "addUser", query = "insert into User (id, name, pass, addr) values(?,?,?,?)", resultClass = User.class)
@Entity

public class User {
	@Id
	private int id;
	private String name;
	private String pass;
	private String addr;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	

}
