package com.client;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.config.HibernateUtil;
import com.model.Student;



public class Test {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		
		System.out.println("**************Insert Data*****************");
		
		
		Query<Student> query1 = session.getNamedNativeQuery("insertData");
		query1.setParameter(0 , 1);
		query1.setParameter(1, "aa");
		query1.executeUpdate();
		tx.commit();
		
		
		
		
		
		
		
	}

}
