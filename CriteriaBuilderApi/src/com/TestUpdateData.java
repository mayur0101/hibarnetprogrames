package com;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class TestUpdateData {
	
	public static void main(String[] args) {
		
		Session sess = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sess.beginTransaction();
//		Update User set name = ? where uid = ?
		
		CriteriaBuilder cb = sess.getCriteriaBuilder();
		CriteriaUpdate<User> cu = cb.createCriteriaUpdate(User.class);
		Root<User> userRoot = cu.from(User.class);
		cu.set(userRoot.get("name"), "Shubham");
		cu.where(cb.equal(userRoot.get("uid"), 3));
		
		Query<User> query = sess.createQuery(cu);
		query.executeUpdate();
		tx.commit();
		
		
	}

}
