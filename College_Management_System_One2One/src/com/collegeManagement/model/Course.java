package com.collegeManagement.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
@NamedNativeQuery(name = "addCourse", query = "insert into Course (cid,cname) values (?, ?)", resultClass = Course.class)
@NamedQueries({
	@NamedQuery(name = "getSingleCourse", query = "from Course Where cid = ?"),
	@NamedQuery(name = "getAll", query = "from Course"),
	@NamedQuery(name = "updateCourse", query = "update Course set cname = ? Where cid = ?"),
	@NamedQuery(name = "deleteCourse", query = "delete from Course Where cid = ?")
})
@Entity
public class Course {
	@Id
	private int cid;
	private String cname;
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}

}
