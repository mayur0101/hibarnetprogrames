package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
@NamedQueries({
	@NamedQuery(name = "getAllData", query = "from Student"),
	@NamedQuery(name = "updateData", query = "Update Student set name = :nm Where rollNo = :rn"),
	@NamedQuery(name = "getSingleData", query = "from Student Where rollNo = :rn")
})
@Entity
public class Student {
	@Id
	private int rollNo;
	private String name;
	
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
