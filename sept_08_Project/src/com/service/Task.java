package com.service;

public interface Task {
	
	void addUser();
	void getSingleUser(int id);
	void getAllUser();
	void updateUser();
	void deleteUser();

}
