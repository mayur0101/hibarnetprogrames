package com.client;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.config.HibernateUtil;
import com.model.Student;

public class Test {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Session sess = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sess.beginTransaction();
		
		System.out.println("***********Insert data************");
		
		System.out.println("Enter Roll No : ");
		int rollNo = sc.nextInt();
		System.out.println("Enter Name : ");
		String name = sc.next();
		
		Query<Student> query = sess.getNamedNativeQuery("insertData");
		query.setParameter(0, rollNo);
		query.setParameter(1, name);
		query.executeUpdate();
		tx.commit();
		
		System.out.println("**************Get All Data****************");
		Query<Student> query2 = sess.getNamedNativeQuery("getAllData");
		List<Student> slist = query2.getResultList();
		slist.forEach(x-> {
			System.out.println(x.getRollNo());
			System.out.println(x.getName());
		});
		
	}

}
