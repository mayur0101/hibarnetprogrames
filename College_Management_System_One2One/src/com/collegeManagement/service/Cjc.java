package com.collegeManagement.service;

public interface Cjc {
	
	void addCourse();
	void viewCourse(int cid);
	void addFaculty();
	void viewFaculty(int fid);
	void addBatch();
	void viewBatch(int bid);
	void addStudent();
	void viewStudent(int sid);

}
