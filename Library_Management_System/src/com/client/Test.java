package com.client;



import java.util.Scanner;

import com.services.Books;
import com.services.LibraryDataOperations;
import com.services.Authors;
import com.services.Books;
import com.services.Category;

public class Test {


		    public static void main(String[] args) {
		        Scanner sc = new Scanner(System.in);
		        LibraryDataOperations bk = new Books();
		        LibraryDataOperations authr = new Authors();
		        LibraryDataOperations ctgry = new Category();
		        boolean a = true;
		        
		        while (a) {
		            System.out.println("------------------------------------------------------------------------------------------------------");
		            System.out.println("***Select*** \n"
		                    + "1. Add Book data \n"
		                    + "2. Add author data \n"
		                    + "3. Add category   \n"
		                    + "4. Exit  ");
		            System.out.println("------------------------------------------------------------------------------------------------------");
		            
		            // Read the user's choice as a String
//		            String input = scanner.nextLine();
		           
		            int ch = 0;

		            if(sc.hasNextInt()) 
		            {
		               ch = sc.nextInt();
		            }

		            
		            try {
//		                int ch = Integer.parseInt(input); // Try to parse the input as an integer

		                switch (ch) {
		                    case 1:
		                        bk.add_data();
		                        break;
		                    case 2:
		                        authr.add_data();
		                        break;
		                    case 3:
		                        ctgry.add_data();
		                        break;
		                    case 4:
		                        a = false;
		                        break;
		                    default:
		                        System.out.println("Invalid input");
		                        break;
		                }
		            } catch (NumberFormatException e) {
		                System.out.println("Please enter a valid integer.");
		            }
		        }

		        // Close the scanner when you're done with it
		        sc.close();
		    }
		
	}

