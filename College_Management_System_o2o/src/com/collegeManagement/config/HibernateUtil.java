package com.collegeManagement.config;

import java.util.HashMap;
import java.util.Map;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.MySQL55Dialect;

import com.collegeManagement.model.Batch;
import com.collegeManagement.model.Course;
import com.collegeManagement.model.Faculty;
import com.collegeManagement.model.Student;

public class HibernateUtil {
	private static SessionFactory sf;
	private static StandardServiceRegistry registry;
	public static SessionFactory getSessionFactory()
	{
		try {
			if(sf==null)
			{
				// connection properties
				Map<String,String> map=new HashMap<>();
				map.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				map.put(Environment.URL, "jdbc:mysql://localhost:3306/NamedQuery");
				map.put(Environment.USER, "root");
				map.put(Environment.PASS, "root");
				//hibernate propeties
				map.put(Environment.DIALECT, "org.hibernate.dialect.MySQLDialect");
				map.put(Environment.HBM2DDL_AUTO, "update");
				map.put(Environment.SHOW_SQL,"true");
				
				
				// create object of StandardServiceRegistryBuilder
				registry=new StandardServiceRegistryBuilder().applySettings(map).build();
				// create object of MetaDataSources
				MetadataSources mds=new MetadataSources(registry);
				mds.addAnnotatedClass(Student.class);
				mds.addAnnotatedClass(Course.class);
				mds.addAnnotatedClass(Faculty.class);
				mds.addAnnotatedClass(Batch.class);
				// create object of MetaData
				  Metadata md  =  mds.getMetadataBuilder().build();
				  // create object of SessionFactory
				  sf=md.getSessionFactoryBuilder().build();
			}
		}
			catch (Exception e) {
				e.printStackTrace();
			}
		
		return sf;
	}

}
