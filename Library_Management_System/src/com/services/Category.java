package com.services;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

//import DatabaseConnector.ConnectionProvider;

public class Category implements LibraryDataOperations {
	private String categoryname;
	@Override
	public void add_data() {
		
	
		try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter Category: ");
            categoryname = scanner.nextLine().trim(); 
            

            if (categoryname.isEmpty()) {
                System.out.println("category name cannot be empty.");
                return;
            }

            String insertcategorySQL = "INSERT INTO category (CategoryName) VALUES (?)";

                      
            try {
            	Class.forName ("com.mysql.jdbc.Driver");
         	   final String JDBC_URL = "jdbc:mysql://localhost:3306/lms?characterEncoding=utf8";
         	
         	 Connection connection=DriverManager.getConnection(JDBC_URL,"root","root");
         	PreparedStatement preparedStatement = connection.prepareStatement(insertcategorySQL);
                preparedStatement.setString(1, categoryname);
                preparedStatement.executeUpdate();
                preparedStatement.close();
                connection.close();
                System.out.println("Category added successfully!");
            } catch (SQLException e) {
            	System.out.println(e);
            } catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
            	System.out.println(e);
			}
           
		
	}
		 
}
}


