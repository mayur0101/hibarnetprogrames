package com;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

public class TestGetAllData {
	
	public static void main(String[] args) {
		
//		Select * from User
		
		Session sess = HibernateUtil.getSessionFactory().openSession();
		
//		Step 1 : create Object of CriteriaBuilder
		CriteriaBuilder cb = sess.getCriteriaBuilder();
//		Step 2 : create object of criteria Query or criteriaUpdate or criteriaDelete
			CriteriaQuery<User> cq = cb.createQuery(User.class);
//		Step 3 : fetch user entity class
			Root<User> userRoot = cq.from(User.class);
//		Step 4 : Select User Entity Data
			cq.select(userRoot);
//		Step 5 : Create Object of Query
			Query<User> query = sess.createQuery(cq);
			List<User> list = query.getResultList();
			list.forEach(x-> {
				System.out.println(x.getUid());
				System.out.println(x.getName());
			});
		
	}

}
