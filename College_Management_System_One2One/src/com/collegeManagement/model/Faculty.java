package com.collegeManagement.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
@NamedNativeQuery(name = "addFaculty", query = "insert into Faculty (fid, fname, Course) values (?, ?, ?)", resultClass = Course.class)
@NamedQueries({
	@NamedQuery(name = "getSingleFaculty", query = "from Faculty Where fid = ?"),
	@NamedQuery(name = "getAll", query = "from Course"),
	@NamedQuery(name = "updateFacultyData", query = "update Faculty set fname = ? Where fid = ?"),
	@NamedQuery(name = "updateFacultyCourseData", query = "update Faculty set Course = ? Where fid = ?"),
	@NamedQuery(name = "deleteFaculty", query = "delete from Course Where fid = ?")
})
@Entity
public class Faculty {
	@Id
	private int fid;
	private String fname;
	@OneToOne(cascade = CascadeType.ALL)
	private Course course;
	
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}

}
