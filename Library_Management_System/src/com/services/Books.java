package com.services;
import java.sql.*;
import java.util.Scanner;


public class Books  implements LibraryDataOperations{
	
	//private int bookid;
	private String title;
	private String authorname;
	private int publicationyear;
	private String categoryname;
	private int quantity;
	

	@Override
	public void add_data() {
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Title of the book ");
		title=sc.next();
		System.out.print("Enter authorname of book ");
		authorname=sc.next();
		System.out.print("Enter publicationyear of the book ");
		publicationyear=sc.nextInt();
		System.out.print("Enter category of the book ");
		categoryname=sc.next();
		System.out.print("Enter quantity of the book ");
		quantity=sc.nextInt();
		
		String insertBookSQL = "INSERT INTO Books (Title, AuthorID, PublicationYear, CategoryID, Quantity) " +
		        "VALUES (?, (SELECT AuthorID FROM Authors WHERE AuthorName = ?), ?, " +
		        "(SELECT CategoryID FROM Categories WHERE CategoryName = ?), ?)";
		
		try {
			Class.forName ("com.mysql.jdbc.Driver");
       	   final String JDBC_URL = "jdbc:mysql://localhost:3306/lms?characterEncoding=utf8";
       	
       	 Connection connection=DriverManager.getConnection(JDBC_URL,"root","root");
            PreparedStatement preparedStatement = connection.prepareStatement(insertBookSQL);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, authorname);
            preparedStatement.setInt(3, publicationyear);
            preparedStatement.setString(4, categoryname);
            preparedStatement.setInt(5, quantity);

            int rowsInserted = preparedStatement.executeUpdate();

            if (rowsInserted > 0) {
                System.out.println("A new book was inserted successfully!");
            }

            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
        	System.out.println(e);
        }catch (ClassNotFoundException e) {
        	System.out.println(e);
		}
		
		
	}

}

/*

String insertBookSQL = "INSERT INTO Books (Title, AuthorID, ISBN, PublicationYear, GenreID, Quantity, ShelfLocation) " +
        "VALUES (?, (SELECT AuthorID FROM Authors WHERE AuthorName = ?), ?, ?, " +
        "(SELECT GenreID FROM Genres WHERE GenreName = ?), ?, ?)";

// Create a PreparedStatement
PreparedStatement preparedStatement = connection.prepareStatement(insertBookSQL);
preparedStatement.setString(1, title);
preparedStatement.setString(2, authorName);
preparedStatement.setString(3, isbn);
preparedStatement.setInt(4, publicationYear);
preparedStatement.setString(5, genreName);
preparedStatement.setInt(6, quantity);
preparedStatement.setString(7, shelfLocation);

// Execute the SQL statement
preparedStatement.executeUpdate();

// Close the PreparedStatement and Connection
preparedStatement.close();
connection.close();  */